a = int(input())
b = int(input())
c = int(input())
d = int(input())

sumi = a + b + c + d
avg = sumi / 4
mul = a * b * c * d
maxi = max(a, b, c, d)
mini = min(a, b, c, d)

print(f"Sum : {format(sumi, '6f')}")
print(f"Average : {format(avg, '6f')}")
print(f"Product : {format(mul, '6f')}")
print(f"MAX : {format(maxi, '6f')}")
print(f"MIN : {format(mini, '6f')}")




