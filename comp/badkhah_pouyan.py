p, d = tuple(map(int, input().split()))
# p, d = (8, 7)
i = 1

while (d * i % p > p / 2) or (d * i % p < 0):
    i += 1

print(d * i)
