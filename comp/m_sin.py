# link
# https://quera.ir/problemset/contest/28947/%D8%B3%D8%A4%D8%A7%D9%84-%D9%BE%DB%8C%D8%A7%D8%AF%D9%87-%D8%B3%D8%A7%D8%B2%DB%8C-%D8%A7%D9%85%D8%B3%DB%8C%D9%86
import random

m = int(input())
# m = 7

haft_seen = [
    'sabze', 'samanoo', 'senjed', 'serkeh',
    'seeb', 'seer', 'somaq'
]

for _ in range(m):
    print(haft_seen.pop(haft_seen.index(random.choice(haft_seen))))
