zarf_1, null_1, zarf_3, null_4 = tuple(map(int, input().split()))

persons = {
    'jamshid': 0,
    'farshid': 0,
    'mahshid': 0,
    'noushid': 0
}

while True:
    if zarf_1 > 0:
        if zarf_3 == 0:
            break
        else:
            persons['jamshid'] += 1
            zarf_1 -= 1

    if zarf_3 > 0:
        if zarf_1 == 0:
            break
        else:
            persons['farshid'] += 1
            zarf_3 -= 1

    if zarf_1 > 0:
        if zarf_3 == 0:
            break
        else:
            persons['mahshid'] += 1
            zarf_1 -= 1

    if zarf_3 > 0:
        if zarf_1 == 0:
            break
        else:
            persons['noushid'] += 1
            zarf_3 -= 1


for k, v in persons.items():
    print(v, end=" ")

print()
