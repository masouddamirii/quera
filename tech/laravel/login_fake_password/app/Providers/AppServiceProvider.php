<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Services\Auth\SillyGuard;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
      $this->registerPolicies();

      Auth::extend('silly', function ($app, $name, array $config) {
          return new SillyGuard(Auth::createUserProvider($config['provider']));
      });
    }
}
