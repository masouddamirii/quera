<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header"><?php echo e($problem->subject); ?></div>

                    <div class="card-body">
                        <?php echo nl2br(e($problem->content)); ?>

                    </div>
                </div>
                <br>
                <?php if(auth()->guard()->check()): ?>
                    <form method="post" action="<?php echo e(route('add_submission', [$problem->id])); ?>">
                        <?php echo csrf_field(); ?>
                        <div class="card">
                            <div class="card-header">Add Submission</div>

                            <div class="card-body">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="answer">
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Submit!" class="btn">
                                </div>
                            </div>
                        </div>
                    </form>
                <?php endif; ?>
            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>