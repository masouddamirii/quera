<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Scoreboard</div>

                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Rank</th>
                            <th scope="col">User</th>
                            <th scope="col">Number of Accepted Problems</th>
                            <th scope="col">Penalty</th>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $__currentLoopData = $users; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $user): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr id="user-<?php echo e($user->id); ?>">
                                <th scope="col" class="user--rank">
                                    <?php echo e($user->rank); ?>

                                </th>
                                <td class="user--name">
                                    <?php echo e($user->name); ?>

                                </td>
                                <td class="user--accepts">
                                    <?php echo e($user->accepts); ?>

                                </td>
                                <td class="user--penalty">
                                    <?php echo e($user->penalty); ?>

                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>