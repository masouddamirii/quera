<?php $__env->startSection('content'); ?>
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Your Submissions</div>

                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Problem</th>
                            <th scope="col">Answer</th>
                            <th scope="col">Status</th>
                        </tr>
                        </thead>
                        <tbody>

                        <?php $__currentLoopData = $submissions; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $submission): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                            <tr id="submission-<?php echo e($submission->id); ?>">
                                <th scope="col"><?php echo e($submission->id); ?></th>
                                <td class="submission--problem-title">
                                    <a href="<?php echo e(route('problem', [$submission->problem->id])); ?>"><?php echo e($submission->problem->subject); ?></a>
                                </td>
                                <td class="submission--answer"><?php echo e($submission->answer); ?></td>
                                <td class="submission--status">
                                    <?php if($submission->status_text == 'not judged'): ?>
                                        In queue...
                                    <?php elseif($submission->status_text == 'accepted'): ?>
                                        Yes! Accepted!
                                    <?php elseif($submission->status_text == 'wrong'): ?>
                                        Oppps! Wrong Answer!
                                    <?php else: ?>
                                        Unknown :\
                                    <?php endif; ?>
                                </td>
                            </tr>
                        <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
<?php $__env->stopSection(); ?>

<?php echo $__env->make('layouts.app', \Illuminate\Support\Arr::except(get_defined_vars(), array('__data', '__path')))->render(); ?>