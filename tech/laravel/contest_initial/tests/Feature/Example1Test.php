<?php

namespace Tests\Feature;

use App\Database\Problem;
use App\Database\Submission;
use App\Database\User;
use Carbon\Carbon;
use Laravel\BrowserKitTesting\TestCase as BaseTestCase;
use Illuminate\Foundation\Testing\RefreshDatabase;
use Symfony\Component\Routing\Matcher\UrlMatcher;
use Tests\CreatesApplication;
use Tests\TestCase;

class ExampleTest extends BaseTestCase
{
    use CreatesApplication;
    use RefreshDatabase;

    public $baseUrl = 'http://localhost';

    private $user1;
    private $user2;
    private $user3;
    private $user4;
    private $problem1;
    private $problem2;
    private $problem3;

    public function setUp()
    {
        parent::setUp();
        User::truncate();
        usleep(100000);
        $this->user1 = new User;
        $this->user1->name = 'ali';
        $this->user1->email = 'ali@gmail.com';
        $this->user1->password = bcrypt('salam1234');
        $this->user1->saveOrFail();
        usleep(50000);
        $this->user2 = new User;
        $this->user2->name = 'reza';
        $this->user2->email = 'reza@gmail.com';
        $this->user2->password = bcrypt('salam1234');
        $this->user2->saveOrFail();
        usleep(50000);
        $this->user3 = new User;
        $this->user3->name = 'amir';
        $this->user3->email = 'amir@gmail.com';
        $this->user3->password = bcrypt('salam1234');
        $this->user3->saveOrFail();
        usleep(50000);
        $this->user4 = new User;
        $this->user4->name = 'hossein';
        $this->user4->email = 'hossein@gmail.com';
        $this->user4->password = bcrypt('salam1234');
        $this->user4->saveOrFail();
        usleep(50000);
        Problem::truncate();
        usleep(100000);
        $this->problem1 = new Problem;
        $this->problem1->subject = "p1";
        $this->problem1->content = "soale 1";
        $this->problem1->saveOrFail();
        usleep(50000);
        $this->problem2 = new Problem;
        $this->problem2->subject = "p2!";
        $this->problem2->content = "soale 2";
        $this->problem2->saveOrFail();
        usleep(50000);
        $this->problem3 = new Problem;
        $this->problem3->subject = "p3";
        $this->problem3->content = "soale 3";
        $this->problem3->saveOrFail();
        usleep(50000);
    }

    public function testProblems()
    {
        $visit = $this->visit('/');
        $visit->assertResponseStatus(200);
        $visit->see("p1");
        $visit->see("p2!");
        $visit->see("p3");
    }

    public function testProblemGuest()
    {
        $visit = $this->visitRoute('problem', [$this->problem2->id]);
        $visit->assertResponseStatus(200);
        $visit->see("p2!");
        $visit->dontSee("p1");
        $visit->dontSee("soale 1");
        $visit->see("soale 2");
        $visit->dontSee("Add Submission");
    }

    public function testProblemUser()
    {
        $visit = $this->actingAs($this->user1)->visitRoute('problem', [$this->problem3->id]);
        $visit->assertResponseStatus(200);
        $visit->see("p3");
        $visit->dontSee("p1");
        $visit->dontSee("soale 1");
        $visit->see("soale 3");
        $visit->see("Add Submission");
    }

    public function testSubmissionsGuest()
    {
        $response = $this->call('GET', '/submissions/');

        $this->assertEquals(403, $response->status());
    }

    public function testAddSubmissionsGuest()
    {
        $response = $this->call('POST', '/problem/' . $this->problem1->id . '/new_submission', [
            'answer' => 'alaki'
        ]);

        $this->assertEquals(403, $response->status());
    }

    public function testAddSubmission()
    {
        $visit = $this->actingAs($this->user1)->visitRoute('problem', [$this->problem2->id]);
        $visit->assertResponseStatus(200);
        $visit->see("Add Submission");
        $addSubmission = $visit->type('some ans', 'answer')
            ->press('Submit!');
        $addSubmission->assertResponseOk();
        $addSubmission->seePageIs('/submissions');
        $this->assertEquals(1, Submission::all()->count());
        $submission = Submission::all()->first();
        $this->assertEquals($submission->problem_id, $this->problem2->id);
        $this->assertEquals($submission->user_id, $this->user1->id);
        $this->assertEquals($submission->status, 0);
        $this->assertEquals($submission->answer, "some ans");
    }

    public function testSubmissions()
    {
        Submission::truncate();
        usleep(100000);
        $u3 = [];
        $u4 = [];
        for ($i = 0; $i < 3; $i++) {
            $submission = new Submission;
            $submission->user_id = $this->user4->id;
            $submission->problem_id = $this->problem1->id;
            $submission->answer = 'alaki(' . $i . ')';
            $submission->submitted_at = Carbon::createFromTimeString(env('CONTEST_START_TIME'))->addMinutes($i * $i + 1);
            $submission->status = $i;
            $submission->save();
//            $submission->mins = $i*$i+1;
            $u4[] = $submission;
            usleep(50000);
        }
        for ($i = 6; $i < 8; $i++) {
            $submission = new Submission;
            $submission->user_id = $this->user3->id;
            $submission->problem_id = $this->problem2->id;
            $submission->answer = 'alaki(' . $i . ')';
            $submission->submitted_at = Carbon::createFromTimeString(env('CONTEST_START_TIME'))->addMinutes($i * $i + 1);
            $submission->status = 0;
            $submission->save();
            $submission->status = 0;
            $u3[] = $submission;
            usleep(50000);
        }
        for ($i = 10; $i < 11; $i++) {
            $submission = new Submission;
            $submission->user_id = $this->user4->id;
            $submission->problem_id = $this->problem2->id;
            $submission->answer = 'alaki(' . $i . ')';
            $submission->submitted_at = Carbon::createFromTimeString(env('CONTEST_START_TIME'))->addMinutes($i * $i + 1);
            $submission->status = 0;
            $submission->save();
            $submission->mins = $i*$i+1;
            $u4[] = $submission;
            usleep(50000);
        }
        $visit1 = $this->actingAs($this->user3)->visitRoute('submissions');
        $visit1->seeElementCount('tr', 3);
        foreach ($u3 as $submission) {
            $p = Problem::find($submission->problem_id);
            $visit1->seeInElement("#submission-{$submission->id}>.submission--problem-title", $submission->problem_id);
            $visit1->seeInElement("#submission-{$submission->id}>.submission--answer", $submission->answer);
            if ($submission->status == 0)
                $visit1->seeInElement("#submission-{$submission->id}>.submission--status", "In queue...");
            elseif ($submission->status == 1)
                $visit1->seeInElement("#submission-{$submission->id}>.submission--status", "Yes! Accepted!");
            elseif ($submission->status == 2)
                $visit1->seeInElement("#submission-{$submission->id}>.submission--status", "Oppps! Wrong Answer!");
  //          $visit1->seeInElement("#submission-{$submission->id}>.submission--submit", $submission->mins . " mins");

        }

        $visit2 = $this->actingAs($this->user4)->visitRoute('submissions');
        $visit2->seeElementCount('tr', 5);
        foreach ($u4 as $submission) {
            $p = Problem::find($submission->problem_id);
            $visit2->seeInElement("#submission-{$submission->id}>.submission--problem-title", $submission->problem_id);
            $visit2->seeInElement("#submission-{$submission->id}>.submission--answer", $submission->answer);
            if ($submission->status == 0)
                $visit2->seeInElement("#submission-{$submission->id}>.submission--status", "In queue...");
            elseif ($submission->status == 1)
                $visit2->seeInElement("#submission-{$submission->id}>.submission--status", "Yes! Accepted!");
            elseif ($submission->status == 2)
                $visit2->seeInElement("#submission-{$submission->id}>.submission--status", "Oppps! Wrong Answer!");
//            $visit1->seeInElement("#submission-{$submission->id}>.submission--submit", $submission->mins . " mins");
        }


    }

    public function testScoreboardEmpty()
    {
        Submission::truncate();
        usleep(100000);
        $visit = $this->visitRoute('scoreboard');
        $visit->seeElementCount('tbody>tr', 4);
        $u = [$this->user1->id, $this->user2->id, $this->user3->id, $this->user4->id];
        foreach ($u as $id) {
            $visit->seeInElement("#user-{$id} .user--rank", 1);
            $visit->seeInElement("#user-{$id} .user--accepts", 0);
            $visit->seeInElement("#user-{$id} .user--penalty", 0);
        }
    }

    private function __addSubmission($problem, $user, $answer, $status, $mins)
    {
        $submission = new Submission;
        $submission->user_id = $user->id;
        $submission->problem_id = $problem->id;
        $submission->answer = $answer;
        $submission->submitted_at = Carbon::createFromTimeString(env('CONTEST_START_TIME'))->addMinutes($mins);
        $submission->status = $status;
        $submission->save();
        usleep(50000);
        return $submission;
    }

    public function testScoreboard2()
    {
        Submission::truncate();
        usleep(100000);
        $this->__addSubmission($this->problem1, $this->user2, '1', 1, 5);
        $this->__addSubmission($this->problem2, $this->user2, '1', 1, 10);
        $this->__addSubmission($this->problem3, $this->user2, '1', 1, 20);
        $visit = $this->visitRoute('scoreboard');
        $id = $this->user2->id;
        $visit->seeInElement("#user-{$id} .user--rank", 1);
        $visit->seeInElement("#user-{$id} .user--accepts", 3);
        $visit->seeInElement("#user-{$id} .user--penalty", 35);

    }



    public function testScoreboard3()
    {
        Submission::truncate();
        usleep(100000);
        $this->__addSubmission($this->problem1, $this->user2, '1', 2, 5);
        $this->__addSubmission($this->problem3, $this->user2, '1', 2, 10);
        $this->__addSubmission($this->problem3, $this->user3, '1', 2, 10);
        $this->__addSubmission($this->problem3, $this->user2, '1', 1, 21);
        $visit = $this->visitRoute('scoreboard');
        $id = $this->user2->id;
        $visit->seeInElement("#user-{$id} .user--rank", 1);
        $visit->seeInElement("#user-{$id} .user--accepts", 1);
        $visit->seeInElement("#user-{$id} .user--penalty", 41);

        $this->__addSubmission($this->problem3, $this->user3, '2', 2, 15);
        $this->__addSubmission($this->problem3, $this->user3, '2', 0, 13);
        $this->__addSubmission($this->problem3, $this->user3, '2', 2, 16);
        $this->__addSubmission($this->problem3, $this->user3, '2', 0, 19);
        $this->__addSubmission($this->problem3, $this->user3, '5', 2, 17);
        $this->__addSubmission($this->problem3, $this->user3, '5', 0, 17);
        $this->__addSubmission($this->problem3, $this->user3, '6', 1, 50);
        $this->__addSubmission($this->problem3, $this->user3, '3', 2, 60);
        $this->__addSubmission($this->problem3, $this->user3, '3', 0, 55);
        $visit = $this->visitRoute('scoreboard');
        $id = $this->user3->id;
        $visit->seeInElement("#user-{$id} .user--rank", 2);
        $visit->seeInElement("#user-{$id} .user--accepts", 1);
        $visit->seeInElement("#user-{$id} .user--penalty", 130);


        $this->__addSubmission($this->problem1, $this->user3, '3', 1, 70);

        $visit = $this->visitRoute('scoreboard');
        $id = $this->user3->id;
        $visit->seeInElement("#user-{$id} .user--rank", 1);
        $visit->seeInElement("#user-{$id} .user--accepts", 2);
        $visit->seeInElement("#user-{$id} .user--penalty", 200);


        $id = $this->user2->id;
        $visit->seeInElement("#user-{$id} .user--rank", 2);
        $visit->seeInElement("#user-{$id} .user--accepts", 1);
        $visit->seeInElement("#user-{$id} .user--penalty", 41);


        $id = $this->user1->id;
        $visit->seeInElement("#user-{$id} .user--rank", 3);
        $visit->seeInElement("#user-{$id} .user--accepts", 0);
        $visit->seeInElement("#user-{$id} .user--penalty", 0);

        $id = $this->user4->id;
        $visit->seeInElement("#user-{$id} .user--rank", 3);
        $visit->seeInElement("#user-{$id} .user--accepts", 0);
        $visit->seeInElement("#user-{$id} .user--penalty", 0);

    }
}
