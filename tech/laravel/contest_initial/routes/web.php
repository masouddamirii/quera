<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::get('/', 'ProblemController@problems')->name('problems');

Route::get('problem/{id}', 'ProblemController@problem')->name('problem');

Route::post('problem/{id}/new_submission', 'SubmissionController@add')->name('add_submission');

Route::get('submissions', 'SubmissionController@list')->name('submissions');

Route::get('scoreboard', 'ScoreboardController@list')->name('scoreboard');
