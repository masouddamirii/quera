<?php

namespace App\Http\Controllers;

use App\Database\Problem;
use App\Database\User;

class ProblemController extends Controller
{
    public function problems()
    {
        $problems = Problem::all();
        return view('problems', [
            'problems' => $problems
        ]);
    }

    public function problem($id)
    {
        $problem = Problem::find($id);
        return view('problem', [
            'problem' => $problem
        ]);
    }
}
