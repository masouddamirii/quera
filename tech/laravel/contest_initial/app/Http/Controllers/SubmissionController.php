<?php

namespace App\Http\Controllers;

use App\Database\Problem;
use App\Database\Submission;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class SubmissionController extends Controller
{


    public function list()
    {
        if (Auth::check()) {
            return view('submissions', [
                'submissions' => Submission::where('user_id', Auth::user()->id)->get()
            ]);
        } else {
            return abort('403');
        }
    }

    public function add($problem_id, Request $request) {
        $request->validate([
            'answer' => 'required',
        ]);

        Submission::create([
            'user_id' => \auth()->user()->id,
            'problem_id' => $problem_id,
            'answer' => $request['answer'],
            'status' => 0,
            'submitted_at' => Carbon::now(),
        ]);

        return redirect(route('submissions'));
    }
}
