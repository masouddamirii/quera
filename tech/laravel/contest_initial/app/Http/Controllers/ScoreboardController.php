<?php

namespace App\Http\Controllers;

use App\Database\Problem;
use App\Database\Submission;
use App\Database\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Auth;

class ScoreboardController extends Controller
{
    public function list()
    {
        $users = User::all();

        return view('scoreboard', [
            'users' => $users
        ]);
    }
}
