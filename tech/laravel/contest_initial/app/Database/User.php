<?php

namespace App\Database;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\DB;


class User extends Authenticatable
{
    use Notifiable;

    public function __construct(array $attributes = [])
    {
        parent::__construct($attributes);



    }

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $appends = [
      'accepts', 'penalty', 'rank'
    ];

    public function getAcceptsAttribute()
    {
        $accepts = Submission::where('user_id', $this->id)->where('status', 1)->count();
        return $accepts;
    }

    public function getPenaltyAttribute()
    {
        $wrong_answer = Submission::where('user_id', $this->id)->where('status', 2)->count();
        $submited_anwers = Submission::where('user_id', $this->id)->where('status', 1)->get();
        $penalty = 0;
        foreach ($submited_anwers as $submited_anwer) {
            $penalty += strtotime($submited_anwer->submitted_at);
        }
        $penalty += ($wrong_answer * 20);
        return $penalty;
    }

    public function getRankAttribute()
    {
        $sort = User::all()->sortByDesc('accepts');
        $i = 1;
        foreach ($sort as $user) {
            if ($user->id == $this->id) {
                return $user->id;
            } else {
                $i++;
            }
        }
    }

}
