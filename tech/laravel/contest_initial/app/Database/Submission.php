<?php

namespace App\Database;

use Illuminate\Database\Eloquent\Model;

class Submission extends Model
{

    protected $fillable = [
        'user_id', 'problem_id', 'answer', 'status', 'submitted_at',
    ];

    protected $dates = [
        'submitted_at',
    ];

    public function getStatusTextAttribute()
    {
        if ($this->attributes['status'] == 0) {
            return 'not judged';
        } else if ($this->attributes['status'] == 1) {
            return 'accepted';
        } else if ($this->attributes['status'] == 2) {
            return 'wrong';
        }
    }

    protected $appends = ['status_text'];

    public function problem()
    {
        return $this->belongsTo(Problem::class);
    }

}
