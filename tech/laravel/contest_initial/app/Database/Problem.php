<?php

namespace App\Database;

use Illuminate\Database\Eloquent\Model;

/**
 * @method static find($id)
 */
class Problem extends Model
{
    protected $fillable = [
        'subject', 'content',
    ];

    public static function findOrFail($id)
    {
    }

}
