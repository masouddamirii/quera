<?php

namespace App\Console\Commands;

use App\Database\Submission;
use Illuminate\Console\Command;

class judge extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'judge';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'judge the answers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
        $this->submissions = Submission::where('status', 0)->orderBy('created_at')->get();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        foreach ($this->submissions as $submission) {
            $this->info('Problem ID: ' . $submission->problem_id);
            $this->info('Answer: ' . $submission->answer);
            $judge = $this->ask('Is it OK?');

            if ($judge === 'yes') {
                $submission->status = 1;
                $submission->save();
            } else {
                $submission->status = 2;
                $submission->save();
            }
        }
    }
}
