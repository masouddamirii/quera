@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ $problem->subject }}</div>

                    <div class="card-body">
                        {!! nl2br(e($problem->content)) !!}
                    </div>
                </div>
                <br>
                @auth
                    <form method="post" action="{{ route('add_submission', [$problem->id]) }}">
                        @csrf
                        <div class="card">
                            <div class="card-header">Add Submission</div>

                            <div class="card-body">
                                <div class="form-group">
                                    <input type="text" class="form-control" name="answer">
                                </div>
                                <div class="form-group">
                                    <input type="submit" value="Submit!" class="btn">
                                </div>
                            </div>
                        </div>
                    </form>
                @endauth
            </div>
        </div>
    </div>
@endsection
