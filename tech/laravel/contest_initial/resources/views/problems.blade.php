@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Problems</div>

                    <div class="list-group list-group-flush" id="problem-list">
                        @foreach ($problems as $problem)
                            <a href="{{ route('problem', [$problem->id]) }}"
                               class="list-group-item list-group-item-action">{{ $problem->subject }}</a>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
