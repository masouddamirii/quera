@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Scoreboard</div>

                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">Rank</th>
                            <th scope="col">User</th>
                            <th scope="col">Number of Accepted Problems</th>
                            <th scope="col">Penalty</th>
                        </tr>
                        </thead>
                        <tbody>
                        @foreach ($users as $user)
                            <tr id="user-{{ $user->id }}">
                                <th scope="col" class="user--rank">
                                    {{ $user->rank }}
                                </th>
                                <td class="user--name">
                                    {{ $user->name }}
                                </td>
                                <td class="user--accepts">
                                    {{ $user->accepts }}
                                </td>
                                <td class="user--penalty">
                                    {{ $user->penalty }}
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection
