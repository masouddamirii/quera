@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Your Submissions</div>

                    <table class="table">
                        <thead>
                        <tr>
                            <th scope="col">#</th>
                            <th scope="col">Problem</th>
                            <th scope="col">Answer</th>
                            <th scope="col">Status</th>
                        </tr>
                        </thead>
                        <tbody>

                        @foreach ($submissions as $submission)
                            <tr id="submission-{{ $submission->id }}">
                                <th scope="col">{{ $submission->id }}</th>
                                <td class="submission--problem-title">
                                    <a href="{{ route('problem', [$submission->problem->id]) }}">{{ $submission->problem->subject }}</a>
                                </td>
                                <td class="submission--answer">{{ $submission->answer }}</td>
                                <td class="submission--status">
                                    @if ($submission->status_text == 'not judged')
                                        In queue...
                                    @elseif ($submission->status_text == 'accepted')
                                        Yes! Accepted!
                                    @elseif ($submission->status_text == 'wrong')
                                        Oppps! Wrong Answer!
                                    @else
                                        Unknown :\
                                    @endif
                                </td>
                            </tr>
                        @endforeach
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
@endsection
